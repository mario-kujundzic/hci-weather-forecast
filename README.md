# weather-forecast

> HCI project

For a list of dependencies, check out package.json

## Node packet manager (NPM) is required for installation
https://nodejs.org/en/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081 (check compilation message for changes to port)
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
