// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import Chart from "chart.js"
import Chartkick from "vue-chartkick"

Vue.use(Vuetify)

Vue.use(Chartkick.use(Chart))

Vue.config.productionTip = false

// global variables
const dailyData = Vue.observable({ dailyData: [] })

Object.defineProperty(Vue.prototype, '$dailyData', {
  get () {
    return dailyData.dailyData;
  },
  set (value) {
    dailyData.dailyData = value;
  }
})

const hourlyData = Vue.observable({ hourlyData: [] })

Object.defineProperty(Vue.prototype, '$hourlyData', {
  get () {
    return hourlyData.hourlyData;
  },
  set (value) {
    hourlyData.hourlyData = value;
  }
})

const cityList = Vue.observable({ cityList: [] })

Object.defineProperty(Vue.prototype, '$cityList', {
  get () {
    return cityList.cityList;
  },
  set (value) {
    cityList.cityList = value;
  }
})

const cityData = Vue.observable({ cityData: [] })

Object.defineProperty(Vue.prototype, '$cityData', {
  get () {
    return cityData.cityData;
  },
  set (value) {
    cityData.cityData = value;
  }
})

const dayNumber = Vue.observable({ dayNumber: 1 })

Object.defineProperty(Vue.prototype, '$dayNumber', {
  get () {
    return dayNumber.dayNumber;
  },
  set (value) {
    dayNumber.dayNumber = value;
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vuetify: new Vuetify( {
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#673ab7',
          secondary: '#3f51b5',
          accent: '#673ab7',
          error: '#f44336',
          warning: '#ffc107',
          info: '#607d8b',
          success: '#4caf50'
          },
      },
    },
  
  }),
  components: { App },
  template: '<App/>'
})
