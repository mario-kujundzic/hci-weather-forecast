import Vue from 'vue'
import Router from 'vue-router'

import NewForecast from "../components/NewForecast"
import CurrentForecast from "../components/CurrentForecast"
import Menu from "../components/MenuBar"
import About from "../components/About"
Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'NewForecast',
      component: NewForecast
    },
    {
      path: '/forecast',
      name: 'CurrentForecast',
      component: CurrentForecast
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})
