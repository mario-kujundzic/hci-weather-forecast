import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
export default new Vuetify( {
    theme: {
      dark: true,
      themes: {
        dark: {
            primary: '#673ab7',
            secondary: '#2196f3',
            accent: '#9c27b0',
            error: '#f44336',
            warning: '#ffc107',
            info: '#00bcd4',
            success: '#4caf50'
        },
      },
    },
  
  })